package model;

import java.util.ArrayList;

public class Mobili {
	
	private String nome;
	private String codice;
	private Float prezzo;
	private int id;
	
	public ArrayList<Categorie> categorieMobile = new ArrayList<Categorie>();
	
	public ArrayList<Categorie> getCategorieMobile() {
		return categorieMobile;
	}
	public void setCategorieMobile(Categorie varCat) {
		this.categorieMobile.add(varCat);
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public Float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(Float prezzo) {
		this.prezzo = prezzo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
