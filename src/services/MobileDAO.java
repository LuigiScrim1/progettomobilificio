package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import connessioni.ConnettoreDB;
import model.Categorie;
import model.Mobili;

public class MobileDAO implements DAO<Mobili>{

	@Override
	public Mobili getById(int id) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<Mobili> getAll() throws SQLException {
		ArrayList<Mobili> elenco = new ArrayList<Mobili>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT id, nome, codice, prezzo FROM mobili";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Mobili temp = new Mobili();
       		temp.setId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setPrezzo(risultato.getFloat(4));
       		temp.setCodice(risultato.getString(3));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Mobili t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Mobili t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Mobili t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	public ArrayList<Mobili> tabAppoggio() throws SQLException {
   		ArrayList<Categorie> cat = new ArrayList<>();
   		ArrayList<Mobili> elenco = new ArrayList<>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT categorie.id, categorie.nome, categorie.descrizione, categorie.codice,"
       			+ "mobili.id, mobili.nome, mobili.codice, mobili.prezzo FROM categorie"
       			+ "	JOIN categoriemobili ON categorie.id=categoriemobili.refCat"
       			+ " JOIN mobili ON categoriemobili.refMob = mobili.id";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	
       	while(risultato.next()){
       		Mobili temp = new Mobili();
       		Categorie temp2 = new Categorie();
       		temp2.setId(risultato.getInt(1));
       		temp2.setNome(risultato.getString(2));
       		temp2.setDescrizione(risultato.getString(3));
       		temp2.setCodice(risultato.getString(4));
       		temp.setId(risultato.getInt(5));
       		temp.setNome(risultato.getString(6));
       		temp.setCodice(risultato.getString(7));
       		temp.setPrezzo(risultato.getFloat(8));
//       		temp.setCategorieMobile(temp2);
       		
       		
       		
       		if(elenco.size()>0) {
       			boolean trovato=false;
       			for(int i=0;i<elenco.size();i++) {
       				if(temp.getId()==(elenco.get(i).getId())) {
       					trovato=true;
       					elenco.get(i).setCategorieMobile(temp2);
       				}else {
       					temp.setCategorieMobile(temp2);
       					elenco.add(temp);
       				}
       			}
       		}else {
       			temp.setCategorieMobile(temp2);
       			elenco.add(temp);
			}
//	       			if(trovato) {
//	       				temp.setCategorieMobile(temp2);
//	       			}else {
//	       				
//	       			}
       		
//       		elenco.add(temp);    //Da togliere perch� l'ho gi� aggiunto.   
       	}
		return elenco;
       	
       	
	}
	

}
