package services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.tomcat.util.threads.ResizableExecutor;

import model.Categorie;
import connessioni.ConnettoreDB;

public class CategoriaDAO implements DAO<Categorie>{
	
	@Override
	public Categorie getById(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        
       	String query = "SELECT id, nome, descrizione, codice FROM categorie WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, id);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();
       	
       	Categorie temp = new Categorie ();
       	temp.setId(risultato.getInt(1));
       	temp.setNome(risultato.getString(2));
       	temp.setDescrizione(risultato.getString(3));
       	temp.setCodice(risultato.getString(4));
       	
       	return temp;
		
	}

	@Override
	public ArrayList<Categorie> getAll() throws SQLException {
		ArrayList<Categorie> elenco = new ArrayList<Categorie>();
	       
   		Connection conn = ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT id, nome, descrizione, codice FROM categorie";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Categorie temp = new Categorie();
       		temp.setId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setDescrizione(risultato.getString(3));
       		temp.setCodice(risultato.getString(4));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Categorie t) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean delete(Categorie t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Categorie t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
