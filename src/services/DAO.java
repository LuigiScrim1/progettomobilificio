package services;

import java.sql.SQLException;
import java.util.ArrayList;

public interface DAO<T> {
	
	T getById(int id) throws SQLException; //lo uso per cercare nel DB tramite la select
	ArrayList<T> getAll() throws SQLException;
	void insert (T t) throws SQLException;
	boolean delete(T t) throws SQLException;
	boolean update(T t) throws SQLException;
}
