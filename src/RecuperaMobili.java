

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import model.Categorie;
import model.Mobili;
import services.CategoriaDAO;
import services.MobileDAO;

@WebServlet("/RecuperaMobili")
public class RecuperaMobili extends HttpServlet {

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		MobileDAO MobDAO = new MobileDAO();

		try {
			ArrayList<Mobili> temp = MobDAO.getAll();
			out.print(new Gson().toJson(temp));
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
