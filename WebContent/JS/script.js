function VisualizzaTabella(arrMobili){
	let stringa = "";
	
	for (let i=0;i<arrMobili.length;i++){
		stringa += righe(arrMobili[i]);
	}
	
	$("#elenco-mobili").html(stringa);
}

function righe (mobile){
	let riga = '<tr identificatore ="' + mobile.id + '">';
	riga += '<td> ' + mobile.nome + '</td>';
	riga += '<td>' + mobile.codice + '</td>';
	riga += '<td>' + mobile.prezzo + '</td>';
	riga += '</tr>';
	
	return riga;
}



function VisualizzaTabellaCategorie(arrCat){
	let stringa = "";
	
	for (let i=0;i<arrCat.length;i++){
		stringa += righe(arrCat[i]);
	}
	
	$("#elenco-categorie").html(stringa);
}

function righe (categoria){
	let riga = '<tr data-identificatore ="' + categoria.id + '">';
	riga += '<td> ' + categoria.nome + '</td>';
	riga += '<td>' + categoria.codice + '</td>';
	riga += '<td>' + categoria.descrizione + '</td>';
	riga += '<td> <button type = "button" class="btn btn-primary btn-block" onclick="modificamelo(this)">M</td>'; 
	riga += '<td> <button type = "button" class="btn btn-danger btn-block" onclick="eliminamelo(this)">E</td>'; 
	riga += '</tr>';
	
	return riga;
}

function richiamaTabella(){
	$.ajax(
		{
			url:"http://localhost:8080/Mobilificio/RecuperaMobili",
			method:"POST",
			success: function(esito){
				console.log(esito);
				VisualizzaTabella(esito);
			},
			error: function(errore){
				console.log(errore);
			}
		}
	);
}


function richiamaTabCategorie(){
	$.ajax(
		{
			url:"http://localhost:8080/Mobilificio/RecuperaCategorie",
			method:"GET",
			success: function(esito){
				VisualizzaTabellaCategorie(esito);
			},
			error: function(errore){
				console.log(errore);
			}
		}
	);
}



function modificamelo(pulsante){
	let idSelezionato = $(pulsante).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/CorsoNTTAPI/recuperaCategorie",
				method: "GET",
				data: {
					id: idSelezionato
				},
				success: function(result){
					
					$("#cat_edit").val(result.titolo);
					$("#descrizione_edit").val(result.descrizione);
					$("#modaleModifica").data("identificatore", idSelezionato);

					$("#modaleModifica").modal("show");
					
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}





$(document).ready(
	function(){
		$("#cta-leggi").click(
			function(){
				console.log("Sono stato cliccato");
				richiamaTabella();
			}
		);


	$("#cta-listaCategorie").click(
		function(){
			richiamaTabCategorie();
			console.log("Sono stato cliccato");
		}
	);
	
}
);